<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 16.08.14
 * Time: 1:43
 */

namespace Arilas\ORMTest\Mvc\Controller\Plugin;

use Arilas\ORM\EntityManager;
use Arilas\ORM\Mvc\Controller\Plugin\GetArilas;
use Arilas\ORM\Repository\AbstractRepository;
use Arilas\ORMTest\AbstractTest;
use Arilas\ORMTest\Bootstrap;
use Arilas\ORMTest\Test\Test;
use Zend\Authentication\AuthenticationService;
use Zend\Mvc\Controller\PluginManager;

class GetArilasTest extends \PHPUnit_Framework_TestCase
{
    use AbstractTest;

    public function testGetInstance()
    {
        $sm = Bootstrap::getServiceManager();
        /** @var PluginManager $pluginManager */
        $pluginManager = $sm->get('Zend\Mvc\Controller\PluginManager');
        /** @var GetArilas $arilas */
        $arilas = $pluginManager->get('getArilas');

        $this->assertInstanceOf(EntityManager::class, $arilas->getManager());
        $this->assertInstanceOf(AuthenticationService::class, $arilas->getAuthenticationService());
        $this->assertInstanceOf(AbstractRepository::class, $arilas->getRepository(Test::class));

        $test = new Test();
        $test->setValue('test');
        $arilas->commit($test);

        $this->assertNotNull($test->getId());
    }
}
 