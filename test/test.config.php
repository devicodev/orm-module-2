<?php
/**
 * Created by PhpStorm.
 * User: gandza
 * Date: 11/20/13
 * Time: 5:18 PM
 */
return array(
    'doctrine' => array(
        'connection' => array(
            // Configuration for service `doctrine.connection.orm_default` service
            'orm_default' => array(
                // connection parameters, see
                // http://docs.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html
                'params' => array(
                    'host' => (getenv('WERCKER_MYSQL_HOST'))?getenv('WERCKER_MYSQL_HOST'):'localhost',
                    'port' => (getenv('WERCKER_MYSQL_PORT'))?getenv('WERCKER_MYSQL_PORT'):'3306',
                    'user' => (getenv('WERCKER_MYSQL_USERNAME'))?getenv('WERCKER_MYSQL_USERNAME'):'root',
                    'password' => (getenv('WERCKER_MYSQL_PASSWORD')) ? getenv('WERCKER_MYSQL_PASSWORD') : 'freeware',
                    'dbname' => (getenv('WERCKER_MYSQL_DATABASE'))?getenv('WERCKER_MYSQL_DATABASE'):'orm-test',
                )
            ),
        ),

        'authentication' => array(
            'orm_default' => array(
                'identity_class' => 'Arilas\ORMTest\Test\Test',
                'identity_property' => 'value',
                'credential_property' => 'value',
                'credential_callable' => function ($user, $passwordGiven) {
                        return true;
                    }
            ),
        ),
        'driver' => array(
            'app_entities' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/ORMTest/Test'
                ),
            ),
            'orm_default' => array(
                // By default, the ORM module uses a driver chain. This allows multiple
                // modules to define their own entities
                'class' => 'Doctrine\ORM\Mapping\Driver\DriverChain',

                // Map of driver names to be used within this driver chain, indexed by
                // entity namespace
                'drivers' => array(
                    'Arilas\ORMTest\Test' => 'app_entities'
                ),
            ),
        ),
    ),
    'session' => array(
        'remember_me_seconds' => 2419200,
        'use_cookies' => true,
        'cookie_httponly' => true,
    ),
    'service_manager' => array(
        'aliases' => array(
            'em' => 'doctrine.entitymanager.orm_default'
        ),
    ),
);
