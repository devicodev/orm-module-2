<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 14.08.14
 * Time: 3:14
 */

namespace Arilas\ORM\Service;


use Arilas\ORM\EntityManager;
use Arilas\ORM\Exception\RuntimeException;
use Arilas\ORM\Repository\AbstractRepository;

class RepositoryFactory
{
    /** @var  AbstractRepository[] */
    protected $instances = [];
    /** @var  EntityManager */
    protected $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getRepository($className)
    {
        if (!isset($this->instances[$className])) {
            $this->createInstance($className);
        }

        return $this->instances[$className];
    }

    protected function createInstance($className)
    {
        if (class_exists($className)) {
            $metadata = $this->entityManager->getClassMetadata($className);
            $repositoryClassName = $metadata->customRepositoryClassName;
            if (is_null($repositoryClassName)) {
                $repository = new AbstractRepository($this->entityManager, $metadata);
            } else {
                $repository = new $metadata->customRepositoryClassName($this->entityManager, $metadata);
            }

            $this->instances[$className] = $repository;
        } else {
            throw new RuntimeException(
                'Entity with class Name:' . $className . ' not found, check your Configuration'
            );
        }
    }

    public function clearObjects()
    {
        foreach ($this->instances as $repository) {
            $repository->getUnitOfWork()->clear();
        }
    }
} 