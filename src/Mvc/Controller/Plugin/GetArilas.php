<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 16.08.14
 * Time: 1:21
 */

namespace Arilas\ORM\Mvc\Controller\Plugin;

use Arilas\ORM\Entity\EntityInterface;
use Arilas\ORM\EntityManager;
use Doctrine\Common\Persistence\ManagerRegistry;
use Zend\Authentication\AuthenticationService;
use Zend\Mvc\Controller\AbstractController;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Mvc\Controller\PluginManager;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class GetEntityManager
 * @package Arilas\ORM\Form\Mvc\Plugin
 * @method AbstractController getController()
 */
class GetArilas extends AbstractPlugin implements ServiceLocatorAwareInterface, ManagerRegistry
{
    /** @var  EntityManager */
    protected $em;
    /** @var  AuthenticationService */
    protected $authenticationService;
    /** @var  PluginManager */
    protected $pluginManager;

    public function commit(EntityInterface $entity)
    {
        $this->getManager()->commit($entity);
    }

    /**
     * @param null $name
     * @return EntityManager
     */
    public function getManager($name = null)
    {
        if (is_null($this->em)) {
            $this->em = $this->getServiceLocator()->getServiceLocator()->get('arilas.orm.entity_manager');
        }

        return $this->em;
    }

    /**
     * Get service locator
     *
     * @return PluginManager
     */
    public function getServiceLocator()
    {
        return $this->pluginManager;
    }

    /**
     * @return AuthenticationService
     */
    public function getAuthenticationService()
    {
        if (is_null($this->authenticationService)) {
            $this->authenticationService = $this->getServiceLocator()->getServiceLocator()
                ->get('arilas.orm.authentication_service');
        }

        return $this->authenticationService;
    }

    /**
     * @param string $entityName
     * @param null   $managerName
     * @return \Arilas\ORM\Repository\AbstractRepository
     */
    public function getRepository($entityName, $managerName = null)
    {
        return $this->getManager()->getRepository($entityName);
    }

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->pluginManager = $serviceLocator;
    }

    /**
     * Gets the default connection name.
     *
     * @return string The default connection name.
     */
    public function getDefaultConnectionName()
    {
        return 'orm_default';
    }

    /**
     * Gets the named connection.
     *
     * @param string $name The connection name (null for the default one).
     *
     * @return object
     */
    public function getConnection($name = null)
    {
        $this->getManager()->getConnection();
    }

    /**
     * Gets an array of all registered connections.
     *
     * @return array An array of Connection instances.
     */
    public function getConnections()
    {
        return [
            $this->getDefaultConnectionName() => $this->getConnection(),
        ];
    }

    /**
     * Gets all connection names.
     *
     * @return array An array of connection names.
     */
    public function getConnectionNames()
    {
        return [
            $this->getDefaultConnectionName(),
        ];
    }

    /**
     * Gets the default object manager name.
     *
     * @return string The default object manager name.
     */
    public function getDefaultManagerName()
    {
        return 'orm_default';
    }

    /**
     * Gets an array of all registered object managers.
     *
     * @return \Doctrine\Common\Persistence\ObjectManager[] An array of ObjectManager instances
     */
    public function getManagers()
    {
        return [
            $this->getDefaultManagerName() => $this->getManager(),
        ];
    }

    /**
     * Resets a named object manager.
     *
     * This method is useful when an object manager has been closed
     * because of a rollbacked transaction AND when you think that
     * it makes sense to get a new one to replace the closed one.
     *
     * Be warned that you will get a brand new object manager as
     * the existing one is not useable anymore. This means that any
     * other object with a dependency on this object manager will
     * hold an obsolete reference. You can inject the registry instead
     * to avoid this problem.
     *
     * @param string|null $name The object manager name (null for the default one).
     *
     * @return \Doctrine\Common\Persistence\ObjectManager
     */
    public function resetManager($name = null)
    {
        $this->getManager()->clear();
        return $this->getManager();
    }

    /**
     * Resolves a registered namespace alias to the full namespace.
     *
     * This method looks for the alias in all registered object managers.
     *
     * @param string $alias The alias.
     *
     * @return string The full namespace.
     */
    public function getAliasNamespace($alias)
    {
        return $alias;
    }

    /**
     * Gets all connection names.
     *
     * @return array An array of connection names.
     */
    public function getManagerNames()
    {
        return [
            $this->getDefaultManagerName(),
        ];
    }

    /**
     * Gets the object manager associated with a given class.
     *
     * @param string $class A persistent object class name.
     *
     * @return \Doctrine\Common\Persistence\ObjectManager|null
     */
    public function getManagerForClass($class)
    {
        return $this->getManager();
    }
}