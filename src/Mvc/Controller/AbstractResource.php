<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 9/23/14
 * Time: 4:51 PM
 */

namespace Arilas\ORM\Mvc\Controller;


use Arilas\ORM\Exception\NotFoundException;
use Arilas\ORM\Mvc\Controller\Plugin\GetArilas;
use Krona\CommonModule\Form\Type\AbstractType;
use Krona\CommonModule\Mvc\AbstractController;
use Krona\CommonModule\Mvc\ActionExecutor;
use Zend\Http\Request;
use Zend\Mvc\Exception;
use Zend\Mvc\MvcEvent;

/**
 * Class AbstractResource
 * @package Arilas\ORM\Mvc\Controller
 * @method GetArilas getArilas()
 * @method AbstractType createInputFilter($entity)
 */
class AbstractResource extends AbstractController
{
    use ActionExecutor;

    protected $identifierName = 'id';

    public function onDispatch(MvcEvent $e)
    {
        $routeMatch = $e->getRouteMatch();
        if (!$routeMatch) {
            /**
             * @todo Determine requirements for when route match is missing.
             *       Potentially allow pulling directly from request metadata?
             */
            throw new Exception\DomainException(
                'Missing route matches; unsure how to retrieve action'
            );
        }

        $request = $e->getRequest();

        try {
            // Was an "action" requested?
            $action = $routeMatch->getParam('action', false);
            if ($action) {
                // Handle arbitrary methods, ending in Action
                $method = static::getMethodFromAction($action);
                if (!method_exists($this, $method)) {
                    $method = 'notFoundAction';
                }

                $return = $this->execute($method);
                $e->setResult($return);
                return $return;
            }

            // RESTful methods
            $method = strtolower($request->getMethod());
            switch ($method) {
                // DELETE
                case 'delete':
                    $id = $this->getIdentifier($routeMatch, $request);
                    if ($id !== false) {
                        $action = 'delete';
                        $return = $this->execute('delete');
                        break;
                    }

                    $action = 'deleteList';
                    $return = $this->execute('deleteList');
                    break;
                // GET
                case 'get':
                    $id = $this->getIdentifier($routeMatch, $request);
                    if ($id !== false) {
                        $action = 'get';
                        $return = $this->execute('get');
                        break;
                    }
                    $action = 'getList';
                    $return = $this->execute('getList');
                    break;
                // HEAD
                case 'head':
                    $id = $this->getIdentifier($routeMatch, $request);
                    if ($id === false) {
                        $id = null;
                    }
                    $action = 'head';
                    $this->execute('head');
                    $response = $e->getResponse();
                    $response->setContent('');
                    $return = $response;
                    break;
                // OPTIONS
                case 'options':
                    $action = 'options';
                    $this->execute('options');
                    $return = $e->getResponse();
                    break;
                // PATCH
                case 'patch':
                    $id = $this->getIdentifier($routeMatch, $request);

                    if ($id !== false) {
                        $action = 'patch';
                        $return = $this->execute('patch');
                        break;
                    }

                    // TODO: This try-catch should be removed in the future, but it
                    // will create a BC break for pre-2.2.0 apps that expect a 405
                    // instead of going to patchList
                    try {
                        $action = 'patchList';
                        $return = $this->execute('patchList');
                    } catch (Exception\RuntimeException $ex) {
                        $response = $e->getResponse();
                        $response->setStatusCode(405);
                        return $response;
                    }
                    break;
                // POST
                case 'post':
                    $action = 'create';
                    $return = $this->execute('create');
                    break;
                // PUT
                case 'put':
                    $id = $this->getIdentifier($routeMatch, $request);

                    if ($id !== false) {
                        $action = 'update';
                        $return = $this->execute('update');
                        break;
                    }

                    $action = 'replaceList';
                    $return = $this->execute('replaceList');
                    break;
                // All others...
                default:
                    $response = $e->getResponse();
                    $response->setStatusCode(405);
                    return $response;
            }
            $routeMatch->setParam('action', $action);
            $e->setResult($return);
        } catch (NotFoundException $exception) {
            $return = $this->notFoundAction();
            $e->setResult($return);
        }

        return $return;
    }

    /**
     * Retrieve the identifier, if any
     *
     * Attempts to see if an identifier was passed in either the URI or the
     * query string, returning it if found. Otherwise, returns a boolean false.
     *
     * @param  \Zend\Mvc\Router\RouteMatch $routeMatch
     * @param  Request $request
     * @return false|mixed
     */
    protected function getIdentifier($routeMatch, $request)
    {
        $identifier = $this->getIdentifierName();
        $id = $routeMatch->getParam($identifier, false);
        if ($id !== false) {
            return $id;
        }

        $id = $request->getQuery()->get($identifier, false);
        if ($id !== false) {
            return $id;
        }

        return false;
    }

    /**
     * Retrieve the route match/query parameter name containing the identifier
     *
     * @return string
     */
    public function getIdentifierName()
    {
        return $this->identifierName;
    }
} 