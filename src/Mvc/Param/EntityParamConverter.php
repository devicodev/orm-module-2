<?php
/**
 * User: krona
 * Date: 16.01.15
 * Time: 1:35
 */

namespace Arilas\ORM\Mvc\Param;

use Arilas\ORM\Entity\EntityInterface;
use Arilas\ORM\EntityManager;
use Arilas\ORM\Mvc\Param\Annotation\EntityConverter;
use Krona\CommonModule\Mvc\Param\AbstractParamConverter;
use Krona\CommonModule\Reflection\ReflectionMethod;
use Zend\Mvc\Controller\AbstractController;

class EntityParamConverter extends AbstractParamConverter
{
    public function canConvert(
        \ReflectionParameter $parameter,
        AbstractController $controller,
        ReflectionMethod $method = null
    ) {
        return !!(!is_null($parameter->getClass()) && $parameter->getClass()->implementsInterface(EntityInterface::class));
    }

    public function convert(
        \ReflectionParameter $parameter,
        AbstractController $controller,
        ReflectionMethod $method = null
    ) {
        if (is_null($parameter->getClass()) || !$parameter->getClass()->implementsInterface(EntityInterface::class)) {
            return null;
        }
        /** @var EntityConverter $annotation */
        $annotation = $method->getReader()->getMethodAnnotation($method, EntityConverter::class);
        if (!$annotation || $annotation->parameter != $parameter->getName()) {
            $annotation = new EntityConverter();
            if ($controller->getEvent()->getRouteMatch()->getParam($parameter->getName(), false)) {
                $annotation->paramName = $parameter->getName();
            }
        }

        /** @var EntityManager $em */
        $em = $this->getServiceLocator()->get(EntityManager::class);
        $repository = $em->getRepository($parameter->getClass()->getName());

        switch($annotation->paramType) {
            case EntityConverter::TYPE_QUERY:
                $id = $controller->getRequest()->getQuery()->get($annotation->paramName);
                break;
            case EntityConverter::TYPE_POST:
                $id = $controller->getRequest()->getPost()->get($annotation->paramName);
                break;
            case EntityConverter::TYPE_ROUTE:
            default:
                $id = $controller->getEvent()->getRouteMatch()->getParam($annotation->paramName);
        }

        $entity = $repository->find($id);
        if ($entity) {
            return $entity;
        } else {
            return null;
        }
    }
}