<?php
/**
 * User: krona
 * Date: 16.01.15
 * Time: 1:37
 */

namespace Arilas\ORM\Mvc\Param\Annotation;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * Class EntityConverter
 * @package Arilas\ORM\Mvc\Param\Annotation
 * @Annotation
 * @Target({"METHOD"})
 */
class EntityConverter
{
    const TYPE_ROUTE = 'route';
    const TYPE_QUERY = 'query';
    const TYPE_POST = 'post';

    public $parameter;

    public $paramType = self::TYPE_ROUTE;

    public $paramName = 'id';
}