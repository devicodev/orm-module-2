<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 16.08.14
 * Time: 3:06
 */

namespace Arilas\ORM\View\Strategy;


use Arilas\ORM\Entity\EntityInterface;
use Arilas\ORM\EntityManager;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\JsonModel;

class EntityStrategy implements ListenerAggregateInterface
{
    protected $listeners = [];

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the EventManager
     * implementation will pass this to the aggregate.
     *
     * @param EventManagerInterface $events
     *
     * @param int $priority
     * @return void
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $this->listeners[] = $events->getSharedManager()->attach(
            'Zend\Stdlib\DispatchableInterface',
            MvcEvent::EVENT_DISPATCH,
            array($this, 'handleResult'),
            -10
        );
    }

    public function handleResult(MvcEvent $event)
    {
        if ($event->getResult() instanceof EntityInterface) {
            /** @var EntityManager $entityManager */
            $entityManager = $event->getApplication()->getServiceManager()->get('arilas.orm.entity_manager');
            $result = $entityManager->getObjectHydrator()->extract($event->getResult(), false);

            $model = new JsonModel(
                [
                    'success' => true,
                    'data' => $result,
                ]
            );
            $event->setResult($model);
        }
    }

    /**
     * Detach all previously attached listeners
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }
}